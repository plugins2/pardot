<?php
/**
 * Pardot plugin for Craft CMS 3.x
 *
 * Send lead to pardot
 *
 * @link      http://croy.solutions/
 * @copyright Copyright (c) 2020 Craig Roy
 */

namespace croy37\pardot\models;

use Craft;
use craft\base\Model;

/**
 * Pardot Settings Model
 *
 * This is a model used to define the plugin's settings.
 *
 * Models are containers for data. Just about every time information is passed
 * between services, controllers, and templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 *
 * @author    Craig Roy
 * @package   Pardot
 * @since     1.0.0
 */
class Settings extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $pardotPassword;

    /**
     * @var string
     */
    public $pardotUserKey;

    /**
     * @var string
     */
    public $fromEmail;

    /**
     * @var string
     */
    public $fromName;

    /**
     * @var string
     */
    public $campaignId;

    /**
     * @var string
     */
    public $listId;

    /**
     * @var string
     */
    public $emailTemplateId;

    /**
     * @var string
     */
    public $campaignIdFr;

    /**
     * @var string
     */
    public $listIdFr;

    /**
     * @var string
     */
    public $emailTemplateIdFr;

    // Public Methods
    // =========================================================================

    /**
     * Returns the validation rules for attributes.
     *
     * Validation rules are used by [[validate()]] to check if attribute values are valid.
     * Child classes may override this method to declare different validation rules.
     *
     * More info: http://www.yiiframework.com/doc-2.0/guide-input-validation.html
     *
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'string'],
            ['pardotPassword', 'string'],
            ['pardotUserKey', 'string'],
            ['fromEmail', 'string'],
            ['fromName', 'string'],
            ['campaignId', 'string'],
            ['listId', 'string'],
            ['emailTemplateId', 'string'],
            ['campaignIdFr', 'string'],
            ['listIdFr', 'string'],
            ['emailTemplateIdFr', 'string'],
        ];
    }

    /**
     * @return string the parsed secret key (e.g. 'XXXXXXXXXXX')
     */
    public function getPardotUserKey()
    {
        $appSecrets = getenv('APP_SECRETS');

        if ($appSecrets !== false) {
            $secrets = json_decode(file_get_contents(getenv('APP_SECRETS')), true);

            return $secrets['CUSTOM']['PARDOT_USER_KEY'];
        }

        return Craft::parseEnv($this->pardotUserKey);
    }

    /**
     * @return string the parsed secret key (e.g. 'XXXXXXXXXXX')
     */
    public function getPardotPassword()
    {
        $appSecrets = getenv('APP_SECRETS');

        if ($appSecrets !== false) {
            $secrets = json_decode(file_get_contents(getenv('APP_SECRETS')), true);

            return base64_decode($secrets['CUSTOM']['PARDOT_PASSWORD']);
        }

        return Craft::parseEnv($this->pardotPassword);
    }
}
