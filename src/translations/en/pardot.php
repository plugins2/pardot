<?php
/**
 * Pardot plugin for Craft CMS 3.x
 *
 * Send lead to pardot
 *
 * @link      http://croy.solutions/
 * @copyright Copyright (c) 2020 Craig Roy
 */

/**
 * Pardot en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('pardot', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Craig Roy
 * @package   Pardot
 * @since     1.0.0
 */
return [
    'Pardot plugin loaded' => 'Pardot plugin loaded',
];
