<?php
/**
 * Pardot plugin for Craft CMS 3.x
 *
 * Send lead to pardot
 *
 * @link      http://croy.solutions/
 * @copyright Copyright (c) 2020 Craig Roy
 */

namespace croy37\pardot\controllers;

use croy37\pardot\Pardot;

use Craft;
use craft\web\Controller;
use croy37\pardot\services\LeadService;
use croy37\pardotsubscribe\services\SubscribeService;
use yii\web\Response;

/**
 * LeadController Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    Craig Roy
 * @package   Pardot
 * @since     1.0.0
 */
class LeadController extends Controller
{

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = true;

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/pardot-subscribe/pardot-subscribe/do-something
     *
     * @return mixed
     */
    public function actionSubscribe()
    {
        /**
         * @var LeadService $service
         */
        $service = new LeadService();

        $email  = Craft::$app->request->getParam('email', '');
        $result = $service->subscribe($email);

        return $this->asJson(array_merge(['success' => true], $result));
    }
}
