<?php
/**
 * Pardot plugin for Craft CMS 3.x
 *
 * Send lead to pardot
 *
 * @link      http://croy.solutions/
 * @copyright Copyright (c) 2020 Craig Roy
 */

namespace croy37\pardot\services;

use Craft;
use craft\base\Component;
use croy37\pardot\models\Settings;
use croy37\pardot\Pardot;
use HGG\Pardot\Connector;
use HGG\Pardot\Exception\PardotException;

/**
 * LeadService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Craig Roy
 * @package   Pardot
 * @since     1.0.0
 */
class LeadService extends Component
{
    public function subscribe(string $email)
    {
        /** @var Settings $settings */
        $settings = Pardot::$plugin->settings;

        $locale = Craft::$app->locale->getLanguageID();

        $connectorParameters = [
            'email'    => $settings->email,
            'user-key' => $settings->getPardotUserKey(),
            'password' => $settings->getPardotPassword(),
            'format'   => 'json',
            'output'   => 'full',
            'version'  => 4
        ];

        $pardotConnector = new Connector($connectorParameters);
        $campaignId      = $locale == 'en' ? $settings->campaignId : $settings->campaignIdFr;
        $emailTemplateId = $locale == 'en' ? $settings->emailTemplateId : $settings->emailTemplateIdFr;
        $listId          = $locale == 'en' ? $settings->listId : $settings->listIdFr;

        //Create prospect
        $prospect = $pardotConnector->post(
            'prospect',
            'create',
            [
                'email'           => $email,
                'campaign_id'     => $campaignId,
                'list_' . $listId => 1
            ]
        );

        //Send Double opt-in email to new prospect
        $pardotConnector->post(
            'email',
            'send',
            [
                'campaign_id'       => $campaignId,
                'prospect_id'       => $prospect['id'],
                'email_template_id' => $emailTemplateId,
                'from_name'         => $settings->fromEmail,
                'from_email'        => $settings->fromName
            ]
        );

        return $prospect;
    }
}
